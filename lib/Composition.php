<?php namespace SteeveDroz\CiComposition;

use Config\Database;

/**
 * This library allows to fetch objects connected with foreign keys to a given object.
 */
class Composition {
    /**
     * If the direction is reversed.
     */
    protected $reversed;

    /**
     * The table considered as "main" in the relationship.
     */
    protected $mainTable;

    /**
     * The primary key of the main table.
     */
    protected $mainPrimaryKey;

    /**
    * The table considered as "auxiliary" in the relationship.
    */
    protected $auxiliaryTable;

    /**
    * The primary key of the auxiliary table.
    */
    protected $auxiliaryPrimaryKey;

    /**
     * The way the relation table is constructed.
     */
    protected $relationTemplate = '{main}_{auxiliary}';

    /**
     * The id of the current object.
     */
    protected $id;

    /**
     * Creates a new Composition about an object.
     *
     * @param string $id The id of the object.
     * @param bool $reversed If true, the object is in the table that is considered as "auxiliary".
     */
    public function __construct(string $id, bool $reversed = false)
    {
        $this->id = $id;
        $this->reversed = $reversed;
    }

    /**
     * Sets a main key of the form "table.key".
     *
     * @param string $mainKey The key of the main table, for example: "users.id".
     * @return Composition This object, for method chaining.
     */
    public function setMainKey(string $mainKey): Composition
    {
        $elements = explode('.', $mainKey);
        if (count($elements) != 2)
        {
            throw new \Error('$mainKey must have the form "table.key", for exemple: "users.id"');
        }

        $this->mainTable = $elements[0];
        $this->mainPrimaryKey = $elements[1];

        return $this;
    }


    /**
    * Sets an auxiliary key of the form "table.key".
    *
    * @param string $auxiliaryKey The key of the auxiliary table, for example: "users.id".
    * @return Composition This object, for method chaining.
    */
    public function setAuxiliaryKey(string $auxiliaryKey): Composition
    {
        $elements = explode('.', $auxiliaryKey);
        if (count($elements) != 2)
        {
            throw new \Error('$auxiliaryKey must have the form "table.key", for exemple: "users.id"');
        }

        $this->auxiliaryTable = $elements[0];
        $this->auxiliaryPrimaryKey = $elements[1];

        return $this;
    }

    /**
     * Changes the way relations are represented in the database.
     *
     * By default, the relation tables are of the form {main}_{auxiliary}. For example, the relationship between users and articles they wrote would be defined in the table users_articles.
     *
     * @param string $relationTemplate The way this relation is represented in the database. Use the placeholders {main} and {auxiliary} to insert the name of the tables, {Main} and {Auxiliary} to insert ucfirst versions of the table, {MAIN} and {AUXILIARY} to insert upper case versions of the tables, and {_main} and {_auxiliary} to insert lower case versions of the tables.
     * @return Composition This object, for method chaining.
     */
    public function setRelationTemplate(string $relationTemplate): Composition
    {
        $this->relationTemplate = $relationTemplate;
        return $this;
    }

    /**
     * Returns a list of objects that are links by a one-to-many relationship to the main one.
     *
     * @param string $thisForeignKey The key of the object in the auxiliary table.
     * @return array An array containing all the corresponding rows as objects.
     */
    public function oneToMany(string $thisForeignKey): array
    {
        $builder = Database::connect()->table($auxiliaryTable);
        return $builder
            ->where($thisForeignKey, $this->mainPrimaryKey)
            ->get()
            ->getResult();
    }


    /**
    * Returns a list of objects that are links by a many-to-many relationship to the main one.
    *
    * @param string $mainForeignKey The key of the object in the main table.
    * @param string $auxiliaryForeignKey The key of the object in the auxiliary table.
    * @return array An array containing all the corresponding rows as objects.
    */
    public function manyToMany(string $mainForeignKey, string $auxiliaryForeignKey): array
    {
        $relationTable = str_replace('{main}', $this->mainTable, $this->relationTemplate);
        $relationTable = str_replace('{auxiliary}', $this->auxiliaryTable, $relationTable);
        $relationTable = str_replace('{Main}', ucfirst($this->mainTable), $relationTable);
        $relationTable = str_replace('{Auxiliary}', ucfirst($this->auxiliaryTable), $relationTable);
        $relationTable = str_replace('{MAIN}', mb_strtoupper($this->mainTable), $relationTable);
        $relationTable = str_replace('{AUXILIARY}', mb_strtoupper($this->auxiliaryTable), $relationTable);
        $relationTable = str_replace('{_main}', mb_strtolower($this->mainTable), $relationTable);
        $relationTable = str_replace('{_auxiliary}', mb_strtolower($this->auxiliaryTable), $relationTable);

        $mainTable = $this->reversed ? $this->auxiliaryTable : $this->mainTable;
        $auxiliaryTable = $this->reversed ? $this->mainTable : $this->auxiliaryTable;
        $mainPrimaryKey = $this->reversed ? $this->auxiliaryPrimaryKey : $this->mainPrimaryKey;
        $auxiliaryPrimaryKey = $this->reversed ? $this->mainPrimaryKey : $this->auxiliaryPrimaryKey;
        $_mainForeignKey = $this->reversed ? $auxiliaryForeignKey : $mainForeignKey;
        $_auxiliaryForeignKey = $this->reversed ? $mainForeignKey : $auxiliaryForeignKey;

        $builder = Database::connect()->table($relationTable);
        return $builder
            ->select($auxiliaryTable . '.*')
            ->join($auxiliaryTable, $auxiliaryTable . '.' . $auxiliaryPrimaryKey . '=' . $relationTable . '.' . $_auxiliaryForeignKey)
            ->where($relationTable . '.' . $_mainForeignKey, $this->id)
            ->get()
            ->getResult();
    }
}
